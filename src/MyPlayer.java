import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {
    //store last move to speed alpha-beta pruning
    Random rand;
    int numCalls = 0;
    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();
        Move bestMove = search(gameBoard, 6, this.playerNumber); //,ab
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        numCalls++;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        System.out.println("numCalls: " + numCalls);
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {
        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                    // Search down an additional level
                    Move responseMove = search(gameBoard, maxDepth - 1, (playerNumber == 1 ? 2 : 1));
                    thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }
        // Pick the highest value move
        return this.getBestMove(moves);

    }


    /*
    This method evaluates the board of a non-terminal node in the minimax tree.
    The method checks the lowest-row even/odd threat of both players. Note that
    at this point it will be the opposing player's move.
    Case 1a: The parameter player has the lowest even/odd threat, and the opposing
    player doesn't have a threat.
    Return 1a: 0.75
    Case 1b: The opposing player has the lowest even/odd threat, and the parameter
    player doesn't have a threat.
    Return 1b: -0.75
    Case 2a: The parameter player has the lowest even/odd threat, and the opposing
    player has an even/odd threat above the parameter player.
    Return 2a: 0.5
    Case 2b: The opposing player has the lowest even/odd threat, and the parameter
    player has an even/odd threat above the opposing player.
    Return 2b: -0.5
    Case 3: Neither player has an even/odd threat.
    Return 3: 0.0
     */
    public double heuristic(Board gameBoard, int playerNumber) {
        int opposingPlayer;
        if(playerNumber == 1) {
            opposingPlayer = 2;
        } else {
            opposingPlayer = 1;
        }
        //If the opposing player can connect four, return -1.0
        if(canWin(gameBoard,opposingPlayer)) {
            return -1.0;
        }

        int goodThreat = hasThreat(gameBoard, playerNumber);
        int badThreat = hasThreat(gameBoard, opposingPlayer);
        boolean hasGoodThreat = true;
        boolean kindaGoodThreat = false;
        boolean kindaBadThreat = false;
        if(goodThreat < 0 || goodThreat == 7) {
            hasGoodThreat = false;
            if(goodThreat == 7)
                kindaGoodThreat = true;//non-even/odd threat
        }
        boolean hasBadThreat = true;
        if(badThreat < 0 || badThreat == 7) {
            hasBadThreat = false;
            if(badThreat == 7)
                kindaBadThreat = true;//non-even/odd threat
        }
        if(hasGoodThreat) {
            if(!hasBadThreat) {
                if(kindaBadThreat) {
                    //S/W
                    return 0.4;
                }
                //S/N
                return 0.6;//1a: good threat, no bad
            } if(goodThreat < badThreat) {
                //2a: good threat, bad threat above
                //SL/S
                return 0.4;
            }
            //2b: bad threat, good above
            //S/SL
            return -0.5;
        }
        else {//no good threat
            if(!hasBadThreat) {//Case 3: no bad threat
                if(kindaGoodThreat) {
                    if(!kindaBadThreat) {
                        //W/N
                        return 0.2;//kinda good threat, no bad threat
                    }
                    //W/W
                    return -0.1;//since opponents turn, kinda good kinda bad
                } if(kindaBadThreat) {
                    //N/W
                    return -0.3;//no good threat any kind, kinda bad threat
                }
                //N/N
                return 0.0;//no threats
            }
            if(kindaGoodThreat) {
                //W/S
                return -0.5;//bad threat and kinda good threat
            }
            //N/S
            //1b: bad threat, no good
            return -0.7;
        }
    }

    private boolean canWin(Board gameBoard, int playerNumber) {
        for (int i = 0; i < Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus == playerNumber) {
                    // Win
                gameBoard.undoMove(i);
                return true;
            }
            gameBoard.undoMove(i);
        }
        return false;
    }
    //returns row if odd threat for player 1, even threat for player 2, -1 otherwise
    private static int hasThreat(Board gameboard, int playerNumber) {
        int[][] board = gameboard.getBoard();
        int col = 0;
        int num_blanks = 0;
        int row;
        boolean w1;
        boolean hasSomeThreat = false;
        //for each column
        int min = -1;
        boolean hasEvenOdd = false;
        while(col < 7) {
            //find the row of first opening
            row = 0;
            while (row < 7) {
                if(board[row][col] == 0) {
                    break;
                }
                row++;
            }
            //if the opening is above the blank tiles
            if(row < 7 - num_blanks) {
                //place the blank tiles
                for(int i = 0; i < num_blanks; i++) {
                    w1 = gameboard.move(-1, col);
                    if(!w1) {
                        System.out.println("Something went wrong w/ moving");
                    }
                }
                //play the players tile
                w1 = gameboard.move(playerNumber, col);
                if(!w1) {
                    System.out.println("Something went wrong w/ moving");
                }
                //check if the player wins the game
                int res = gameboard.checkIfGameOver(col);
                //undo the last move and all blanks
                for(int i = 0; i < num_blanks + 1; i++) {
                    w1 = gameboard.undoMove(col);
                    if(!w1) {
                        System.out.println("Something went wrong w/ unmoving");
                    }
                }
                if(res == playerNumber) {
                    hasSomeThreat = true;
                    //this means we have a threat, check if it is odd/even,
                    //corresponding to the correct player
                    //player 1 wants odd threats
                    boolean firstPlayerOdd = ((playerNumber == 1) && (row%2 == 0));//since first row is 0
                    //player 2 wants even threats
                    boolean secondPlayerEven = ((playerNumber == 2) && (row%2 == 1));
                    if(firstPlayerOdd || secondPlayerEven) {
                        if(row < min)
                            min = row;
                        hasEvenOdd = true;
                        //return row;
                    }
                }
                //otherwise, update number of blank tiles, and repeat
                num_blanks++;
            } else { //if all rows in column have been checked, move to the next one
                col++;
                num_blanks = 0;
            }
        }
        if(hasEvenOdd)
            return min;
        if(hasSomeThreat)
            return 7;
        //if and odd/even threat is never found, then return -1
        return -1;
    }

    //modified to work from center->outwards
    private Move getBestMove(ArrayList<Move> moves) {
        int center = moves.size()/2;
        Move centerMove = moves.get(center);
        double max = centerMove.value;
        Move bestMove = centerMove;
        int i = 1;
        int j = 1;
        int currIndex = center;
        Move curr;
        boolean done = false;
        while(!done) {
            currIndex = center + j*(i/2);
            if(currIndex < 0 || max == 1.0 || currIndex > moves.size() - 1) {
                done = true;
            } else {
                curr = moves.get(currIndex);
                if(curr.value > max) {
                    max = curr.value;
                    bestMove = curr;
                }
                j*=-1;
                i++;
            }
        }
        return bestMove;
    }



}
